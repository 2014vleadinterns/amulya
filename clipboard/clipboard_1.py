import sys
import tempfile

__all__ = ["copytext", "copyblob", "gettext", "getblob", "reset"]

__text__ = None
__blob__ = None
__limit__ = 30
__temp__ = None
__temp_blob__ = None


def copytext(text):
    global __text__
    global __temp__
   
    __temp__ = tempfile.TemporaryFile()
    if len(text) > __limit__:
        __temp__ = tempfile.TemporaryFile()
	__temp__.write(text)
        __text__ = __temp__.name
        __temp__.seek(0)
    elif len(text) <= __limit__ :
        __text__ = text 
	__temp__ = "hello"
def copyblob(blob):
    global __blob__
    __blob__ = blob
    __temp_blob__ = tempfile.TemporaryFile()
    if len(blob) > __limit__:
        __temp_blob__.write(str(__blob__))
        __blob__ = __temp_blob__.name
        __temp_blob__.seek(0)
    elif len(blob) <= __limit__ :
        __blob__ = blob 

def gettext():
    global __text__
    global __temp__
    if type(__temp__) == file :
	#__temp__.seek(0)
        return __temp__.read()
    elif type(__temp__) == str:
	return __text__

def getblob():
    global __blob__
    global __temp_blob__
    try:
    	__temp_blob__.seek(0)
        return __temp_blob__.read()
    except:
         return __blob__
def reset():
    global __text__, __blob__
    __text__ = None
    __blob__ = None
    

##
## -------------------------------------------------------------
##
__observers__ = []
def addobserver(observer):
    __observers__.append(observer)

def removeobserver(observer):
    try:
        __observers__.remove(observer)
    except ValueError, TypeError:
        pass

def notify(reason):
    for observer in __observers__:
        if observer is not None:
            try:
                observer(reason)
            except TypeError:
                pass
