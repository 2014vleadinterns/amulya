# -*- coding: UTF-8 -*-
import clipboard_1
__limit__ = 10

def run_clipboard_tests():
    def test_empty_clipboard():
        assert(clipboard_1.gettext() == None)
        assert(clipboard_1.getblob() == None)
    
    def test_reset_clipboard():
        clipboard_1.reset()
        assert(clipboard_1.gettext() == None)
        assert(clipboard_1.getblob() == None)

    def test_reset_last_stage():
         blob = clipboard_1.getblob()
         text = clipboard_1.gettext()
         if text is not None and blob is not None:
            clipboard_1.reset()
            blob = clipboard_1.getblob()
            text = clipboard_1.gettext()
            assert(text == None)
            assert(blob == None)
            
    def test_copy_hindi_text():
        clipboard_1.reset()
        msg = u'?? ?? ????'
        clipboard_1.copytext(msg)
        text = clipboard_1.gettext()
        #assert(msg == text)
        assert(len(msg) <= __limit__)

    def test_copy_english_text():
        clipboard_1.reset()
        clipboard_1.copytext("hello1")
        text = clipboard_1.gettext()
        assert(text == "hello1")
        assert(len(text) <= __limit__)

    def test_copy_text_greater_than_limit():
        clipboard_1.reset()
        clipboard_1.copytext("hello, World...!!!!")
        text = clipboard_1.gettext()
        assert(text == "hello, World...!!!!")
        assert(len(text) >= __limit__)

    def test_copy_blob():
        clipboard_1.reset()
        clipboard_1.copyblob([1,2,3])
        blob = clipboard_1.getblob()
        assert(blob == [1,2,3])

    def test_copy_blob_greater_than_limit():
        clipboard_1.reset()
        clipboard_1.copyblob([1,2,3,4,5,6,67,7,8,89,9,4,5])
        blob = clipboard_1.getblob()
        assert(blob == [1,2,3,4,5,6,67,7,8,89,9,4,5])

    def test_copy_text_recursion():
          text = clipboard_1.gettext()
          if text is not None:
              clipboard_1.copytext("hello World");
              text = clipboard_1.gettext()
              assert(text == "hello World")

    def test_copy_blob_recursion():
          blob = clipboard_1.getblob()
          if blob is not None:
              clipboard_1.copyblob([1,3,2,4]);
              text = clipboard_1.getblob()
              assert(text == [1,3,2,4])
    
    def test_copy_text_blob():
        blob = clipboard_1.getblob()
        text = clipboard_1.gettext()
        if text is not None and blob is None:
            clipboard_1.copyblob([1,3,5,3,2])
            blob = clipboard_1.getblob()
            assert(blob == [1,3,5,3,2])
            assert(text == clipboard_1.gettext())

    def test_copy_blob_text():
        blob = clipboard_1.getblob()
 	text = clipboard_1.gettext()
        if text is None and blob is not None:
            clipboard_1.copytext("This is a test sentence") 
	    text = clipboard_1.gettext() 
            assert(text == "This is a test sentence")
            assert(blob == clipboard_1.getblob()) 

    def test_copy_text_blob_text():
        clipboard_1.reset()
        clipboard_1.copytext("Hello World")
        clipboard_1.copyblob([1,2,3,4])
        clipboard_1.copytext("Second sentence")

        text = clipboard_1.gettext()
        blob = clipboard_1.getblob()
        assert(text == "Second sentence")
        assert(blob == [1,2,3,4])

    def test_copy_text_blob_blob():
        clipboard_1.reset()
        clipboard_1.copytext("Hello World")
        clipboard_1.copyblob([1,2,3,4])
        clipboard_1.copyblob([4,5,6,7])

        text = clipboard_1.gettext()
        blob = clipboard_1.getblob()
        assert(text == "Hello World")
        assert(blob == [4,5,6,7])

    def test_copy_blob_text_text():
        clipboard_1.reset()
        clipboard_1.copyblob([1,2,3,4])
        clipboard_1.copytext("Hello World")
        clipboard_1.copytext("Second sentence")
        
        text = clipboard_1.gettext()
        blob = clipboard_1.getblob()
        assert(text == "Second sentence")
        assert(blob == [1,2,3,4])
        
    def test_copy_blob_text_blob():
        clipboard_1.reset()
        clipboard_1.copyblob([1,2,3,4])
        clipboard_1.copytext("Hello World")
        clipboard_1.copyblob([4,5,6,7])

        text = clipboard_1.gettext()
        blob = clipboard_1.getblob()
        assert(text == "Hello World")
        assert(blob == [4,5,6,7]) 

    test_empty_clipboard()    
    test_reset_clipboard()
    test_reset_last_stage()
    test_copy_hindi_text()
    test_copy_english_text()
    test_copy_text_greater_than_limit()
    test_copy_blob()
    test_copy_blob_greater_than_limit()
    test_copy_text_recursion()
    test_copy_blob_recursion()
    test_copy_text_blob()
    test_copy_blob_text()
    test_copy_text_blob_text()
    test_copy_text_blob_blob()
    test_copy_blob_text_text()
    test_copy_blob_text_blob()

run_clipboard_tests()


def run_clipboard_observer_tests():
    def test_one_observer():
        def anobserver(reason):
            print "observer notified. reason: ", reason

        clipboard_1.reset()
        clipboard_1.addobserver(anobserver)
        clipboard_1.copytext("hello, world!")
    
    test_one_observer()

run_clipboard_observer_tests()
