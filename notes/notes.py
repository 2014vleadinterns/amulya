import sys
import re
from xml.etree.ElementTree import Element, SubElement, ElementTree

__notes__ = []
found_notes = []

def clear_notes():
    global __notes__
    __notes__ = []
    
def new_note(title, body):
    if title == " ":
        title = "NULL"
    __notes__.append((title,body))

def delete_note_by_title(title):
    global __notes__
    if title == " ":
        title = "NULL"
    length = len(__notes__)
    for i, (title1,body) in enumerate(__notes__):
        if __notes__[i][0] == title:
           del __notes__[i]	   

#new_note("t1","b1")
#new_note("t2","b2")
#new_note("t3","b3")
#new_note("t1","be")
#delete_note_by_title("t1")

#print __notes__

def find_in_notes(key):
    global found_notes
    found_notes = []
    #for i, (title1,body) in enumerate(__notes__):
    #   if title1.find(key) != -1 or body.find(key) != -1:
    #	    found_notes.append((title1,body))
    found_notes = re.findall(key,str(__notes__))
	
    return found_notes 
 
def save_notes(filepath):
    global __notes__
    
    root = Element('root')
    for i, (title1,body) in enumerate(__notes__):
	note = SubElement(root,"note")
        title_xml = SubElement(note,"title")
        body_xml = SubElement(note,"body")
 	title_xml.text = __notes__[i][0]
 	body_xml.text  = __notes__[i][1]
    	
    tree = ElementTree(root)
    tree.write(filepath);
     
#save_notes("check.xml")
    
def get_notes():
    global __notes__
    return __notes__
