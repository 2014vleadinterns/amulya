import notes
import xml.etree.ElementTree as ET


def run_notes_test_cases():
    def test_clear_notes(): # Clears the notes
        assert(notes.get_notes() == [])
    
    def test_new_note(): # Adds the notes and then checks
        #notes.clear_notes()
        note = notes.get_notes()
        prev_len = len(note)
        notes.new_note("title1","body1")
        length = len(notes.get_notes())
        assert(length == prev_len +1)

    def test_delete_note_by_title():
        note = notes.get_notes()
        prev_len = len(note)
	found_notes = notes.find_in_notes("title1")
        notes.delete_note_by_title("title1")
	len_found_notes = len(found_notes)
        length = len(notes.get_notes())
        assert(length == prev_len - len_found_notes)

    def test_find_in_notes():
        notes.clear_notes()
        notes.new_note("title1","body1")
        found_notes = notes.find_in_notes("title1")
        assert(found_notes == [('title1')])

    def test_new_note_recursive():
        note = notes.get_notes()
        prev_len = len(note)      
        notes.new_note("t1","body1")
	notes.new_note("t2","body1")
        length = len(notes.get_notes())
        assert(length == prev_len +2)
        body = note[length-1][1]
        assert(body == "body1")
        
    def test_delete_note_recursive():
        note = notes.get_notes()
        prev_len = len(note)
	found_notes = notes.find_in_notes("t1")
        f2 = notes.find_in_notes("t2")
        for i in range(1,3):
            notes.delete_note_by_title("t"+str(i))
        assert(len(notes.get_notes()) == prev_len-len(found_notes)- len(f2))

    def test_new_delete_case(): # add new note and then delete new note and find it.
        notes.clear_notes()
        note = notes.get_notes()
        prev_len = len(note)

        notes.new_note("abc","body1 def abc....")
        found_notes = notes.find_in_notes("abc")
        #assert(found_notes == [('abc')])
	notes.delete_note_by_title("abc")
        found_notes = notes.find_in_notes("abc")
        length = len(note)
        
        assert(length == prev_len)
        assert(found_notes == [])

    def test_new_null():# when title is not given...
        note = notes.get_notes()
        prev_len = len(note)
        notes.new_note(" ","body1")
        length = len(notes.get_notes())
        assert(length == prev_len +1)

    def test_delete_note_by_title_null():
        note = notes.get_notes()
        prev_len = len(note)
        notes.delete_note_by_title("")
        length = len(notes.get_notes())
	found_notes = note.find_in_notes("")
        assert(length == prev_len - len(found_notes))

    def test_save_notes():
	note = notes.get_notes()
	tree = ET.parse('check.xml')
	root = tree.getroot()
	count = root.findall('note')
	length = len(note) + 1 
	assert(len(count) == length)
     
    test_clear_notes()
    test_new_note()
    test_delete_note_by_title()
    test_find_in_notes()
    test_new_note_recursive()
    test_delete_note_recursive()
    test_new_delete_case()
    test_new_null()
    test_save_notes()
    
run_notes_test_cases()
