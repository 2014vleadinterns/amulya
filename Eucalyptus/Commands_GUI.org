* EUCALYPTUS

** Using Console.

*** Launching Instances

**** Log on to your User Console.
**** Select "Launch New Instance".
**** In "Create New Instance" wizard, select the "Image" listed. Select "Select Type" to continue.
**** In the next dialog, you can provide details for your selected Instance such as "number of instances" you wish to launch, the "name" of your instance and the desired "size" of it as well. 
**** Select the "Availability Zone" in which you wish to launch your instance.
**** Next, select the correct "Key Pair" and "Security Group" that we created in our earlier steps.
**** Launch Instance.
****
**** Next, launch a Terminal.
**** > chmod 600 <Key_Pair_Name> 
**** > ssh -i <Key_Pair_Name> ec2-user@<Instance_IP>


*** Creating Key-Value Pair :

**** Log on to your User Console with the appropriate Credentials.
**** From the Dashboard, select "Network and Security" >> "Key Pairs".
**** It will display your existing Key Pairs. By default, Eucalyptus Faststart creates two default key pairs named "admin" and "demo". 
**** To create your own Key pair, select "Create new Key Pair".
**** In the "Create New Key Pair" Dialog box, provide a suitable name for your Key Pair and select "Create and Download".
**** You have to save this .pem file to your local machine/ client from where you plan to take SSH access to your Eucalyptus Instances.



*** Creating Security Groups :

**** Log on to your User Console.
**** From the Dashboard, select "Network and Security" >> "Security Groups".
**** This will display your existing Security Groups. By default, Eucalyptus Faststart creates a "default" security group with port 22 (SSH) enabled. 
**** To create your own Security Group, select "Create new Security Group" option.
**** In the "Group" tab, we first provide a "Name" for your security group followed by its "Description". Both the fields are required.
**** In Rules Tab, we can specify which ports to open up for us to communicate with our Instances.
**** Tags simply help you to uniquely identify each "Item", in this case, Security Groups with the help of Key-Value Pairs. 
**** You can provide your own values here if you wish. This is OPTIONAL. Once done, click "Create" to create your Security Group.


*** Creating Volumes :

**** Log on to your User Console and select the "Storage" >> "Volumes" and then select "Create New Volume".
**** Provide a suitable "Name" for your Volume along with a desired Volume "Size".
**** Next important thing is to select the "Availability Zone" in which you wish to provision this Volume.
**** Once filled, select "Create Volume".
**** Make a note of the "Volume ID" as we will need it as a reference when we attach this volume to our running Instance. 
**** in the same dashboard view, select the option "More Actions" >> "Attach to Instance".
