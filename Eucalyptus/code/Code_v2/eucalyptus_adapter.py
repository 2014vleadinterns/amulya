import os
import boto
import sys
import time
import boto.manage.cmdshell
from datetime import date,timedelta
from prettytable import PrettyTable
from subprocess import call
from pprint import pprint
import logging
import ConfigParser


global __conn__
image_list = []

logging.basicConfig(filename='eucalyptus.log',level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s' , datefmt='%m/%d/%Y %I:%M:%S %p')

class eucalyptus_adapter():

	Config = ConfigParser.ConfigParser()
	Config.read("/home/cloud/boto")
	options = Config.options('Credentials')
	access_key =  Config.get('Credentials', 'euca_access_key_id')
	secret_key =  Config.get('Credentials','euca_secret_access_key')

	# Check Connection
	@classmethod
	def get_connection(self,ip,access_key,secret_key):
		logging.info('Establishing Connection ....')
		print "Waiting for connection to establish..."
		global __conn__
		try:
			__conn__ = boto.connect_euca(ip,access_key,secret_key)
        	        return __conn__
		except:
			print " Please enter Valid Credentials"
			return 1
	
	# Print Images List
	@classmethod
	def get_image_list(self):
		global __conn__
	        global image_list
		x = PrettyTable(["Image ID", "Name", "Architecture", "State", "Type"])
		images = __conn__.get_all_images()
		logging.info('Getting Image List...')
		for i in images:
	                if 'emi' in i.id:
	                        l = []
	        	        l.insert(0, i.id)
	        	        l.insert(2, i.architecture)
	                        l.insert(3, i.__dict__.get('state'))
	                        l.insert(4, i.__dict__.get('root_device_type'))
			        l.insert(1, i.name)
				if l[3] != 'failed' and l[3] :
					x.add_row([l[0], l[1], l[2], l[3], l[4]])
					image_list.append(l)       	
		print x
		return len(image_list)

	# Count Number of Images
	@classmethod
	def count_images(self):
		global __conn__
		global image_list
		logging.info('Counting the number of images...')
		return len(image_list)

	# Get particular specs Images
	@classmethod
	def get_specific_image(self, attribute, argument):
		global __conn__
        	global image_list
        	image = []
		x = PrettyTable(["Image ID", "Name", "Architecture", "State", "Type"])
        	#pprint(images[0].__dict__)
		for i in image_list:
			if attribute == 'name':
				if argument in i[1]:
        	                	l = []
        		        	l.insert(0, i[0])
        		        	l.insert(2, i[2])
        	                	l.insert(3, i[3])
        	                	l.insert(4, i[4])
			        	l.insert(1, i[1])
					image.append(l)
					x.add_row([l[0], l[1], l[2], l[3], l[4]])
			elif attribute == 'architecture':
				if argument in i[2]:
					l = []
        		        	l.insert(0, i[0])
        		        	l.insert(2, i[2])
        	                	l.insert(3, i[3])
        	                	l.insert(4, i[4])
			        	l.insert(1, i[1])
					image.append(l)
					x.add_row([l[0], l[1], l[2], l[3], l[4]])
			elif attribute == 'state':
				if argument in i[3]:
					l = []
        		        	l.insert(0, i[0])
        		        	l.insert(2, i[2])
        	                	l.insert(3, i[3])
        	                	l.insert(4, i[4])
			        	l.insert(1, i[1])
					image.append(l)
					x.add_row([l[0], l[1], l[2], l[3], l[4]])
			elif attribute == 'type':
				if argument in i[4]:
					l = []
        		        	l.insert(0, i[0])
        		        	l.insert(2, i[2])
        	                	l.insert(3, i[3])
        	                	l.insert(4, i[4])
			        	l.insert(1, i[1])
					image.append(l)
					x.add_row([l[0], l[1], l[2], l[3], l[4]])
		print x
		return len(image)

	# Count Number of Specific Images
	@classmethod
	def count_specs_images(self, attribute, argument):
		global __conn__
		global image_list
        	count = 0 
		for i in image_list:
			if attribute == 'name':
				if argument in i[1]:
					count = count + 1
			elif attribute == 'architecture':
				if argument in i[2]:
					count = count + 1
			elif attribute == 'state':
				if argument in i[3]:
					count = count + 1
			elif attribute == 'type':
				if argument in i[4]:
					count = count + 1	
		return count

	#Delete a Image
	@classmethod
	def delete_image(self, image_id):
		global __conn__
		images = __conn__.get_all_images()
		flag = 0
		for i in images:
			if i.id in image_id:
				flag =1
				break	
    		if flag == 1:
			logging.info('Deleting the Image....')
			__conn__.deregister_image(image_id)
		else:
			logging.warning('Please Enter Valid Image Id... Image is not deleted...')
			print "Please enter Valid image id "        
		return flag
	
	# Create Instance
	@classmethod
	def create_instance(self, image, key, group, instance):
		global __conn__
		try:	
			reservation = __conn__.run_instances(image_id=image,key_name=key, security_groups=group, instance_type=instance)
			instance = reservation.instances[0]
			logging.info('Waiting for instance to Create...')
			print 'Waiting for instance to create...'
			while instance.state != 'running':
				time.sleep(5)
    				instance.update()
			logging.info('Instance is now Running...')
			print 'Instance is now running' 
			print 'Instance Public IP : %s' % instance.__dict__.get('ip_address')
			print 'Instance Private IP : %s' % instance.__dict__.get('private_ip_address')
			print 'Instance ID : %s' % instance.__dict__.get('id')
			print 'Instance Type : %s' % instance.__dict__.get('instance_type')
			print 'Instance Image ID : %s' % instance.__dict__.get('image_id')
			print "\n"
			#self.ssh_shell(instance.__dict__.get('id'))
			return instance.__dict__.get('id')
		except:
			logging.warning('Instance cannot be created...')
			print "Instance CANNOT be created..."
			return 0
	
	# List of all Instances
	@classmethod
	def get_all_instances(self):
		global __conn__
	        running_instances = []
	        instance = __conn__.get_only_instances()
		logging.info('Counting the number of instances...')
		for i in instance:
			running_instances.append(i)
		return running_instances

	# List of Running Instances
	@classmethod
	def get_running_instances(self):
		global __conn__
        	running_instances = []
		instance = __conn__.get_only_instances()
		logging.info('Counting the number of running instances...')
		for i in instance:
			if i.state == 'running':
				running_instances.append(i)
		return running_instances

	# Validating Instance
	@classmethod
	def validate_instance(self, instance_id):
		global __conn__
        	ins_id = __conn__.get_only_instances(instance_id)
        	count = -1
		logging.info('Validating Instance....')
		for i in ins_id:
			print i.id
			if i.id in instance_id:	
				logging.info('Instance is Valid...')
				print "Instance is Valid"
			else:
				logging.warning('Instance is not Valid')
				print "Instance is not Valid"

	# Starting Instance
	@classmethod 
	def start_instances(self, instance_id):
        	global __conn__
        	instance = __conn__.get_only_instances(instance_id)
		logging.info('Starting Instance....')
		for i in range(len(instance)):
			if instance[i]:
				start = __conn__.start_instances(instance[i].id)
				logging.info('Waiting for Instance to Start....')
				print 'Waiting for instance to start...'
				while instance[i].state != 'running':
		    			time.sleep(5)
		    			instance[i].update()
				logging.info('Instance has now been started....')
				print 'Instance has now been started' 
				print 'Started Instance IP is %s' % instance[i].ip_address
				print 'Started Instance ID is %s' % instance[i].id
	
			else:
				logging.info('Please Enter Valid Instance ID... ')
				print "Please enter valid instance id.."

	# Stopping Instance
	@classmethod
	def stop_instances(self, instance_id):
	        global __conn__
	     	instance = __conn__.get_only_instances(instance_id)
		for i in range(len(instance)): 
			if instance:		
				stop = __conn__.stop_instances(instance[i].id)	
				logging.info('Waiting for instance to stop...')
				print 'Waiting for instance to stop...'
				print 'Stopped Instance IP is %s' % instance[i].ip_address
	                	print 'Stopped Instance ID is %s' % instance[i].id
				while instance[i].state != 'stopped':
	    				time.sleep(5)
	    				instance[i].update()
				logging.info('Instance has now been stopped...')
				print 'Instance has now been stoped' 	
			else :
				logging.info('Please Enter Valid Instance ID...')
				print "Please enter valid instance id.."

	# Restart Instance	
	@classmethod
	def restart_instances(self, instance_id):
	        global __conn__
	        instance = __conn__.get_only_instances(instance_id)
		for i in range(len(instance)):
			if instance:		
				start = __conn__.reboot_instances(instance[i].id)
				logging.info('Waiting for instance to restart...')
				print 'Waiting for instance to start...'
				while instance[i].state != 'running':
	    				time.sleep(5)
	    				instance[i].update()
				logging.info('Instance has now been rebooted...')
				print 'Instance has now been rebooted' 
				print 'Rebooted Instance IP is %s' % instance[i].ip_address
				print 'Rebooted Instance ID is %s' % instance[i].id		
			else :
				logging.info('Please enter Valid Instance id...')
				print "Please enter valid instance id.."

	# Destroy Instance
	@classmethod
	def terminate_instances(self, instance_id):
		global __conn__
		instance = __conn__.get_only_instances(instance_id)
		for i in range(len(instance)):
			if instance:
				terminate = __conn__.terminate_instances(instance[i].id)
				logging.info('Waiting for instance to terminate....')
				print 'waiting for instance to terminate...'
				while instance[i].state != 'terminated':
    					time.sleep(5)
    					instance[i].update()
				logging.info('Instance has now been terminated....')
				print 'Instance has now been terminated' 		
			else:
				logging.info('Please Enter Valid Instance id...')
				print ' Please Enter Valid Instance id '

	# Generate KeyPair
	@classmethod
	def generate_keypair(self, key_name):
		global __conn__
		logging.info('Generating Key pair...')
		print "Generating Keypair..."
        	k_name = __conn__.create_key_pair(key_name)
		return k_name

	# Get KeyPair
	@classmethod
	def get_key_pair(self, key_name):
		global __conn__
		k_name = 0
		key = __conn__.get_all_key_pairs()
		logging.info('Getting Key pair...')
        	for k in key:
        	        if k.name == key_name:
				logging.info('Key pair Matched...')
        	                print "Keypair MATCHED..."
        	                k_name = k
        	                break
        	if k_name == 0:
			logging.warning('Keypair Not Found....Enter Valid name...')
        	        print "Keypair NOT FOUND..."
		return k_name
	
	# Delete KeyPair
	@classmethod
	def delete_key_pair(self, key_name):
		global __conn__
		logging.info('Deleting Key Pair....')
		print "Deleting Keypair..."
	        k_name = __conn__.delete_key_pair(key_name)
		return k_name
	
	# Generate Group
	@classmethod
	def generate_group(self, group_name):
		global __conn__
		logging.info('Generating Group...')
		print "Generating Group..."
	        g_name = __conn__.create_security_group(group_name, 'Security Group')
		a = __conn__.authorize_security_group(group_name=g_name.name, ip_protocol='tcp', from_port=22, to_port=22, cidr_ip='0.0.0.0/0')
		print a
		return g_name
	
	# Get Group
	@classmethod
	def get_group(self, group_name):
		global __conn__
		g_name = 0
		group = __conn__.get_all_security_groups() 
		logging.info('Getting Group...')
		for g in group:
	    		if g.name == group_name:
				logging.info('Group Matched...')
	        		print "Group Matched"
	       			g_name = g
	        		break
		if g_name == 0:  
			logging.info(' Group Not Found... Please enter valid Group name...')
	    		print "Group NOT FOUND..."	
		return g_name
	
	# Delete Image
	@classmethod	
	def delete_group(self, group_name):
		global __conn__
		logging.info('Deleting Group...')
		print "Deleting Group..."
	        g_name = __conn__.delete_security_group(group_name)
		return g_name

	# SSH Shell
	@classmethod	
	def ssh_shell(self, instance_id):
		key_path = os.path.join(os.path.expanduser('~/Downloads'), 'key1.private')
		print key_path
		logging.info(key_path)
		instance = __conn__.get_only_instances(instance_id)[0]
		cmd = boto.manage.cmdshell.sshclient_from_instance(instance, key_path, user_name='ubuntu')
		print cmd
		logging.info(cmd)
	        status, stdin, stderr = cmd.run('df -h')
	        print stdin
	
	# Resource Utilization
	@classmethod	
	def resource_utilization(self):
		today = date.today()
		yesterday = date.today() - timedelta(1)
		str = yesterday.isoformat()
		str1 = today.isoformat()
		logging.info('Generating Report...')
		call(["eureport-generate-report", "-s", str, "-e",str1, "Report.html"])
		logging.info('Report is saved to Report.html..')

	# Instance Status
	@classmethod		
	def get_status(self, instance_id):
		global __conn__
        	instance = __conn__.get_only_instances(instance_id)
		logging.info('Getting status of Instance...')
		for inst in range(len(instance)):
			print "Instance Status: %s" % instance[inst].state
	

	# Instance IP
	@classmethod	
	def get_instance_ip(self, instance_id):
		global __conn__		
	        instance = __conn__.get_only_instances(instance_id)
		logging.info('Getting instance ip...')
		for inst in range(len(instance)):
			print "Instance IP: %s" % instance[inst].ip_address

	# Volume Snapshot
	@classmethod	
	def snapshot_instance(self, volume_id):
		snapshot = __conn__.create_snapshot(volume_id, 'Snap_shot'+volume_id)
		return 'Snap_shot'+volume_id

	# Get RAM Size
	@classmethod
	def get_ram(instance_id):
        	global __conn__
        	a = __conn__.get_all_zones('verbose')
		instance = __conn__.get_only_instances(instance_id)
		type = instance[0].instance_type
		d = { }	
		for i in a:
        	        l = []
        	        l.insert(0, i.name)
        	        l.insert(1, i.state)
			spt_str = str(l[0][3:])
			d[spt_str] = l[1]
	
		flavor = d[type]
		print flavor
		ram_size = flavor.split()[4]
	        return ram_size

	# Get Disk Size
	@classmethod
	def get_disk(instance_id):
		global __conn__
	        a = __conn__.get_all_zones('verbose')
	        instance = __conn__.get_only_instances(instance_id)
	        type = instance[0].instance_type
	        d = { }
	        for i in a:
	                l = []
	                l.insert(0, i.name)
	                l.insert(1, i.state)
			spt_str = str(l[0][3:])
	                d[spt_str] = l[1]
	        flavor = d[type]
		print flavor
        	disk_size = flavor.split()[5]
		return disk_size




		
