import os
import boto
import time
from eucalyptus_adapter import eucalyptus_adapter as adapter
import logging

global conn

logging.basicConfig(filename='eucalyptus.log',level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s' , datefmt='%m/%d/%Y %I:%M:%S %p')

def run_instance_creation_tests():

        def test_get_connection_wrong_ip():
                global conn
                conn = adapter.get_connection('10.2.56.2134','AKIAYL1OGJDNW3VC3OLU','etGkEl9ipIIay0sLhDqRyZsANGCKFdMzM3FRGSUp')
                print "TEST PASSED : Connection FAILED..."
		print "Wrong Cloud IP : %s" % conn.host
		logging.warning('Connection FAILED : Wrong Cloud IP ')
                assert(conn.host == '10.2.56.2134')

	def test_get_connection_wrong_access_key():
		global conn
		conn = adapter.get_connection('10.2.56.20','AKIAJDNW3VC3OLU','etGkEl9ipIIay0sLhDqRyZsANGCKFdMzM3FRGSUp')
		print "TEST PASSED : Connection FAILED..."
		print "Wrong Cloud Access Key : %s" % conn.access_key
		logging.warning('Connection FAILED : Wrong Cloud Access Key')
		assert(conn.access_key == 'AKIAJDNW3VC3OLU')
	
	def test_get_connection_wrong_secret_key():
		global conn
		conn = adapter.get_connection('10.2.56.21','AKIAYL1OGJDNW3VC3OLU','etGkEl9ipIIay0sGCKFdMzM3FRGSUp')
		print "TEST PASSED : Connection FAILED..."
		print "Wrong Cloud Secret Key : %s" % conn.access_key
		logging.warning('Connection FAILED : Wrong Cloud Secret Key')
		assert(conn.secret_key == 'etGkEl9ipIIay0sGCKFdMzM3FRGSUp')		

	def test_get_connection():
		global conn
		conn = adapter.get_connection('10.2.56.20','AKIAYL1OGJDNW3VC3OLU','etGkEl9ipIIay0sLhDqRyZsANGCKFdMzM3FRGSUp')
		print "TEST PASSED : Connection Established SUCCESSFULLY..."
		print "Eucalyptus Cloud IP : %s" % conn.host
		logging.info('Connection Established SUCCESSFULLY...')
		assert(conn.host == '10.2.56.20')

	def test_get_image_list():		
		global conn
		s = adapter.get_image_list()
		print "TEST PASSED : Images FOUND"
		print "\nNumber of Images : %d" % s  
		logging.info('SUCCESSFULLY displayed Image List...')
		assert(s == adapter.count_images()) 

	def test_get_os_specific_image():		
		global conn
		s = adapter.get_specific_image('name', 'eustore')
		print "TEST PASSED : Images FOUND"
		logging.info('SUCCESSFULLY displayed OS Specific Image List...')
		assert(s == adapter.count_specs_images('name', 'eustore')) 
	

	def test_get_arch_specific_image():		
		global conn
		s = adapter.get_specific_image('architecture', 'x86_64')
		print "TEST PASSED : Images FOUND"
		logging.info('SUCCESSFULLY displayed Architecture Specific Image List...')
		assert(s == adapter.count_specs_images('architecture', 'x86_64')) 
	

	def test_get_state_specific_image():		
		global conn
		s = adapter.get_specific_image('state', 'available')
		print "TEST PASSED : Images FOUND"
		logging.info('SUCCESSFULLY displayed State Specific Image List...')
		assert(s == adapter.count_specs_images('state', 'available')) 
	
	
	def test_get_type_specific_image():		
		global conn
		s = adapter.get_specific_image('type', 'ebs')
		print "TEST PASSED : Images FOUND"
		logging.info('SUCCESSFULLY displayed Type Specific Image List...')
		assert(s == adapter.count_specs_images('type', 'ebs')) 

	def test_delete_image_wrong_id():
		global conn
		a = adapter.count_images()
		s = adapter.delete_image('emi-07D535D2') # wrong image id
		if s == 0:
			print "Invalid Image ID : emi-07D535D2"
			print "TEST PASSED : Image NOT DELETED..."
			logging.info('Image NOT DELETED : Invalid Image ID')		
		else:
			print "TEST FAILED : Image DELETED..."
		assert(a == adapter.count_images())
	
	def test_delete_image():
		global conn
		a = adapter.count_images()
		s = adapter.delete_image('emi-7E0B5649') # correct image id
		#s = adapter.get_image_list()
		if s==1:
			print "Valid Image ID : emi-7E0B5649"
			print "TEST PASSED : Image DELETED"
			logging.info('Image Deleted SUCCESSFULLY')	
		else:
			print "TEST FAILED : Image NOT DELETED"
		assert(adapter.count_images() == a-1)

	def test_delete_image_null_id():
		global conn
		a = adapter.count_images()
		s = adapter.delete_image(' ') # null image id
		if s == 0:
			print "Invalid Image ID : "
			print "TEST PASSED : Image NOT DELETED..."
			logging.info('Image NOT DELETED : Invalid Image ID')		
		else:
			print "TEST FAILED : Image DELETED..."
		assert(a == adapter.count_images())

	def test_create_instance():
		a = len(adapter.get_all_instances())
		instance_id = adapter.create_instance('emi-C65144D4','key1',['Group1'],'m1.small')
		assert(len(adapter.get_all_instances()) == a+1)

	def test_create_instance_wrong_emi():
		a = len(adapter.get_all_instances())
		instance_id = adapter.create_instance('emi-035D2','key1',['Group1'],'t1.micro')
		print "Wrong IMAGE ID : emi-035D2"
		assert(instance_id == 0)

	def test_create_instance_wrong_key():
		a = len(adapter.get_all_instances())
		instance_id = adapter.create_instance('emi-07D535D2','k',['Group1'],'t1.micro')
		print "Wrong KEYPAIR : k"
		assert(instance_id == 0)

	def test_create_instance_wrong_group():
		a = len(adapter.get_all_instances())
		instance_id = adapter.create_instance('emi-07D535D2','key1',['G'],'t1.micro')
		print "Wrong GROUP : G"
		assert(instance_id == 0)

	def test_create_instance_wrong_type():
		a = len(adapter.get_all_instances())
		instance_id = adapter.create_instance('emi-07D535D2','key1',['Group1'],'to')
		print "Wrong TYPE : to"
		assert(instance_id == 0)

	def test_validate_instance():
		instance = adapter.validate_instance(['i-74673E40', 'i-74673E40'])
		#assert(instance == 1)

	def test_validate_instance_wrong_id():
		instance = adapter.validate_instance('i-2D52')
		print "INVALID Instance : i-2D52"
		#assert(instance == None)

	def test_validate_instance_null():
		instance = adapter.validate_instance(' ') 
		print "INVALID Instance"
		#assert(instance == None)

	def test_stop_instance():
		instance = adapter.stop_instances(['i-74673E40', 'i-74673E40'])
		#assert(instance == 'stopped')


	def test_stop_instance_wrong_id():
		instance = adapter.stop_instances('i-2D05405')
		print "INVALID Instance : i-2D52"
		#assert(instance == 0)


	def test_stop_instance_null_id():
		instance = adapter.stop_instances(' ')
		print "INVALID Instance"
		#assert(instance == 0)


	def test_start_instance():
		instance = adapter.start_instances(['i-74673E40', 'i-74673E40'])
		#assert(instance == 'running')


	def test_start_instance_wrong_id():
		instance = adapter.start_instances('i-2D05405')
		print "INVALID Instance : i-2D05405"
		#assert(instance == 0)


	def test_start_instance_null_id():
		instance = adapter.start_instances(' ')
		print "INVALID Instance"
		#assert(instance == 0)


	def test_restart_instance():
		instance = adapter.restart_instances(['i-74673E40', 'i-74673E40'])
		#assert(instance == 'running')


	def test_restart_instance_wrong_id():
		instance = adapter.restart_instances('i-2D05405')
		print "INVALID Instance : i-2D52"
		#assert(instance == 0)


	def test_restart_instance_null_id():
		instance = adapter.restart_instances(' ')
		print "INVALID Instance"
		#assert(instance == 0)


	def test_terminate_instance():
		instance = adapter.terminate_instances(['i-74673E40', 'i-74673E40'])
		#assert(instance == 'terminated')

	def test_terminate_instance_wrong_id():
		instance = adapter.terminate_instances('i-2D05405')
		print "INVALID Instance : i-2D05405"
		#assert(instance == 0)


	def test_terminate_instance_null_id():
		instance = adapter.terminate_instances(' ')
		print "INVALID Instance"
		#assert(instance == 0)

	def test_get_key():
		global conn
		key = adapter.get_key_pair('key1')
		print "Keypair Name : %s" % key.name
		assert(key.name == "key1")


	def test_get_wrong_key():
		global conn
		key = adapter.get_key_pair('key')
		print "Keypair Name : key"
		assert(key == 0)

		
	def test_generate_keypair():
		global conn
		key = adapter.generate_keypair('key')
		print "Keypair Name : %s" % key.name
		assert(key.name == 'key')

	
	def test_delete_key_pair():
		global conn
		key = adapter.delete_key_pair('key')
		print "Keypair Deleted : key"  
		assert(key == True)


	def test_get_group():
		global conn
		group = adapter.get_group('Group1')
		print "Group Name : %s" % group.name
		assert(group.name == 'Group1')


	def test_get_wrong_group():
		global conn
		group = adapter.get_group('Group6')
                print "Group Name : Group6"
		assert(group == 0)

	
	def test_generate_group():
		global conn
		group = adapter.generate_group('Group4')
		print "Group Name : %s" % group.name
		assert(group.name == 'Group4')

	
	def test_delete_group():
		global conn
		group = adapter.delete_group('Group4')
		print "Group Deleted : Group4"  
		assert(group == True)

	def test_resource_utilization():	
		print "Resource Utilization"
		adapter.resource_utilization()

		
	def test_ssh_shell():
		print "SSH into Ubuntu Instance"
		adapter.ssh_shell('i-AF0F45B1')



	print '\n------------- Connection Failed IP Test ---------------\n'
	test_get_connection_wrong_ip()

	print '\n--------- Connection Failed ACCESS KEY Test -----------\n'
	test_get_connection_wrong_access_key()

	print '\n--------- Connection Failed SECRET KEY Test -----------\n'
	test_get_connection_wrong_secret_key()

	print '\n------------ Connection Establish Test ----------------\n'
	test_get_connection()

	print '\n---------------- Get Image List Test ------------------\n'
	test_get_image_list()

	print '\n-------------- Get OS Specific Image Test -------------\n'
	test_get_os_specific_image()

	print '\n-------- Get Architecture Specific Image Test ---------\n'
	test_get_arch_specific_image()

	print '\n----------- Get State Specific Image Test -------------\n'
	test_get_state_specific_image()

	print '\n------------ Get Type Specific Image Test -------------\n'
	test_get_type_specific_image()

	print '\n-------------- Delete Wrong Image Test ----------------\n'
	test_delete_image_wrong_id()

	print '\n------------------ Delete Image Test ------------------\n'
	test_delete_image()

	print '\n--------------- Delete Null Image Test ----------------\n'
	test_delete_image_null_id()
	
	print '\n---------------- Create Instance Test -----------------\n'
	test_create_instance()

	print '\n-------- Create Instance With Wrong Image Test --------\n'
	test_create_instance_wrong_emi()

	print '\n--------- Create Instance With Wrong Key Test ---------\n'
	test_create_instance_wrong_key()

	print '\n-------- Create Instance With Wrong Group Test --------\n'
	test_create_instance_wrong_group()

	print '\n--------- Create Instance With Wrong Type Test --------\n'
	test_create_instance_wrong_type()

	print '\n--------------- Validating Instance Test --------------\n'
	test_validate_instance()

	print '\n------------- Validating Wrong Instance Test ----------\n'
	test_validate_instance_wrong_id()

	print '\n------------- Validating Null Instance Test -----------\n'
	test_validate_instance_null()

	print "\n------------------ Stop Instance Test -----------------\n"
	test_stop_instance()

	print '\n-------------- Stopping Wrong Instance Test -----------\n'
	test_stop_instance_wrong_id()

	print '\n--------------- Stopping Null Instance Test -----------\n'
	test_stop_instance_null_id()

	print "\n---------------- Start Instance Test ------------------\n"
	test_start_instance()

	print '\n------------ Starting Wrong Instance Test -------------\n'
	test_start_instance_wrong_id()

	print '\n------------ Starting Null Instance Test --------------\n'
	test_start_instance_null_id()

	print "\n--------------- Restart Instance Test -----------------\n"
	test_restart_instance()

	print '\n----------- Restarting Wrong Instance Test ------------\n'
	test_restart_instance_wrong_id()

	print '\n------------- Restarting Null Instance Test -----------\n'
	test_restart_instance_null_id()

	print "\n--------------- Terminate Instance Test ---------------\n"
	test_terminate_instance()

	print "\n------------ Terminate Wrong Instance Test ------------\n"
	test_terminate_instance_wrong_id()

	print "\n------------ Terminate Null Instance Test -------------\n"
	test_terminate_instance_null_id()

	print '\n------------------- Get keypair Test ------------------\n'
	test_get_key()

	print '\n------------------ Wrong Keypair Test -----------------\n'
	test_get_wrong_key()

	print '\n----------------- Create Keypair Test -----------------\n'
	test_generate_keypair()

	print '\n-------------------- Get Group Test -------------------\n'
	test_get_group()

	print '\n---------------- Deleting Keypair Test ----------------\n'
	test_delete_key_pair()

	print '\n----------------- Get Wrong Group Test ----------------\n'
	test_get_wrong_group()

	print '\n------------------ Create Group Test ------------------\n'
	test_generate_group()

	print '\n------------------ Deleting Group Test ----------------\n'
	test_delete_group()

	print "--------------- Resource Utilization Test ---------------\n"
	test_resource_utilization()

	print "----------------- SSH onto Instance Test ----------------\n"
	test_ssh_shell()

run_instance_creation_tests()
