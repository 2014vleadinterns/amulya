import os
import boto
import time
import instance_creation


global conn

def run_instance_creation_tests():
	def test_get_connection():
		global conn
		conn = instance_creation.get_connection('10.2.56.20','AKIAYL1OGJDNW3VC3OLU','etGkEl9ipIIay0sLhDqRyZsANGCKFdMzM3FRGSUp')
		assert(isinstance(conn,boto.ec2.connection.EC2Connection))

	def test_get_connection_wrong_ip():
		global conn
		conn = instance_creation.get_connection('10.2.56.2134','AKIAYL1OGJDNW3VC3OLU','etGkEl9ipIIay0sLhDqRyZsANGCKFdMzM3FRGSUp')
		print conn
		assert(conn == 1)

	def test_get_connection_wrong_access_key():
		global conn
		conn = instance_creation.get_connection('10.2.56.20','AKIAJDNW3VC3OLU','etGkEl9ipIIay0sLhDqRyZsANGCKFdMzM3FRGSUp')
		assert(conn == 1)

#	def test_get_connection_wrong_secret_key():
#		global conn
#		conn = instance_creation.get_connection('10.2.56.21','AKIAYL1OGJDNW3VC3OLU','etGkEl9ipIIay0sGCKFdMzM3FRGSUp')
#		assert(isinstance(conn,None))

	def test_delete_image():
		global conn
		s = instance_creation.delete_image('emi-4DCA3B0A') # correct image id
		#s = instance_creation.get_image_list()
		if s==1:
			print "Image not deleted... test case failed"
		else:
			print "Image deleted...test case passed"

	def test_delete_image_wrong_id():
		global conn
		s = instance_creation.delete_image('emi-07D535D2') # wrong image id
		if s == 0:
			print "Image not deleted... test case passed"
		else:
			print "Image deleted...test case failed"

	def test_delete_image_null_id():
		global conn
		s = instance_creation.delete_image(' ') # null image id
		if s ==0 :
			print "Image not deleted... test case passed"
		else:
			print "Image deleted...test case failed"
	
	def test_get_image_list():		
		global conn
		s = instance_creation.get_all_images() 
		#assert(len(s) == ) 

	
	def test_get_image():
		global conn
		image = instance_creation.get_image('eustore-trusty-server-cloudimg-amd64_tar')
		assert(isinstance(image,unicode))	

	def test_get_key():
		global conn
		key = instance_creation.get_key_pair('key1')
		assert(key.name == "key1")
	
	def test_get_group():
		global conn
		group = instance_creation.get_group('Group1')
		assert(group.name == 'Group1')		

	def test_create_instance():
		instance_id = instance_creation.create_instance('emi-07D535D2','key1','Group1','t1.micro')
		print instance_id
		assert(isinstance(instance_id,unicode))

	def test_create_instance_wrong_emi():
		instance_id = instance_creation.create_instance('emi-035D2','key1','Group1','t1.micro')
		assert(isinstance(instance_id,None))

	def test_create_instance_wrong_key():
		instance_id = instance_creation.create_instance('emi-07D535D2','k','Group1','t1.micro')
		assert(isinstance(instance_id,None))

	def test_create_instance_wrong_group():
		instance_id = instance_creation.create_instance('emi-07D535D2','key1','G','t1.micro')
		assert(isinstance(instance_id,None))
	def test_create_instance_wrong_type():
		instance_id = instance_creation.create_instance('emi-07D535D2','key1','Group1','to')
		assert(isinstance(instance_id,None))

	def test_validate_instance():
		instance = instance_creation.validate_instance('i-2D054052')
		assert(instance == 1)

	def test_validate_instance_wrong_id():
		instance = instance_creation.validate_instance('i-2D52')
		assert(instance == None)

	def test_validate_instance_null():
		instance = instance_creation.validate_instance(' ') 
		assert(instance == None)


	def test_start_instance():
		instance = instance_creation.start_instance('i-2D054052')
		assert(instance == 'running')

	def test_start_instance_wrong_id():
		instance = instance_creation.start_instance('i-2D05405')
		assert(instance == 0)

	def test_start_instance_null_id():
		instance = instance_creation.start_instance(' ')
		assert(instance == 0) 


	def test_terminate_instance():#correct id
		instance = instance_creation.terminate_instance('i-2D054052')
		assert(instance == 'terminated')

	def test_terminate_instance_wrong_id():#wrong id
		instance = instance_creation.terminate_instance('i-2D05405')
		assert(instance == 0)

	def test_terminate_instance_null_id():#null id
		instance = instance_creation.terminate_instance(' ')
		assert(instance == 0)

		
	print ' ============== Connection Method Test ======================'
	test_get_connection()
	#test_get_image()
	print ' ============== Get Key Test ======================'
	test_get_key()
	print ' ============== Get Group Test ======================'
	test_get_group()
	
#	test_get_connection_wrong_ip()
	print ' ============== Delete Image Test ======================'
	test_delete_image()
	print ' ============== Delete Image wrong id Test ======================'
	test_delete_image_wrong_id()
	print ' ============== Delete image null id ======================'
	test_delete_image_null_id()
#	test_create_instance()
	print ' ============== Validate instance Test ======================'
	test_validate_instance()
	print ' ============== Validate instance wrong id Test ======================'
	test_validate_instance_wrong_id()
	print ' ============== validate instance null id Test ======================'
	test_validate_instance_null()
#	test_start_instance()
	print ' ==============start instance wrong id Test ======================'
	test_start_instance_wrong_id()
	print ' ============== Start instance null id ======================'
	test_start_instance_null_id()
	print ' ============== terminate instance  ======================'
#	test_terminate_instance()
	print ' ============== terminate instance wrong id ======================'
	test_terminate_instance_wrong_id()
	print ' ============== terminate instance null id ======================'
	test_terminate_instance_null_id()

run_instance_creation_tests()



