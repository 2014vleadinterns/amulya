import os
import time
import boto
import sys
from prettytable import PrettyTable

global __conn__
global x
global image


name= ["m1.small","m1.medium","m1.large","m1.xlarge","m3.xlarge","m3.2xlarge","c1.medium","c1.xlarge","cc1.4xlarge","cc2.8xlarge","m2.xlarge","m2.2xlarge","m2.4xlarge","cr1.8xlarge",
"hi1.4xlarge","hs1.8xlarge","t1.micro","cg1.4xlarge"]

disk_size=[5,10,10,10,15,30,10,10,60,120,10,30,60,240,120,24000,5,200]
ram_size = [256,512,512,1024,2048,4096,512,2048,3072,6144,2048,4096,4096,16384,6144,119808,256,16384]


# Check Connection
def get_connection(ip,access_key,secret_key):
	global __conn__
	try:
		__conn__ = boto.connect_euca(ip,access_key,secret_key)
		return __conn__
	except:
		print " Please enter Valid Credentials"
		return 1
        #conn=boto.connect_euca('10.2.56.20','AKIAYL1OGJDNW3VC3OLU','etGkEl9ipIIay0sLhDqRyZsANGCKFdMzM3FRGSUp')
get_connection('10.2.56.20','AKIAYL1OGJDNW3VC3OLU','etGkEl9ipIIay0sLhDqRyZsANGCKFdMzM3FRGSUp')

# Create a Image
def create_image():
	global __conn__
	image_id = __conn__.register_image(name=None, description=None, image_location=None, architecture=None, kernel_id=None, ramdisk_id=None, root_device_name=None, block_device_map=None, dry_run=False, virtualization_type=None,  sriov_net_support=None, snapshot_id=None, delete_root_volume_on_termination=False)
	

#Delete a Image
def delete_image(image_id):
	global __conn__
	images = __conn__.get_all_images()
	flag = 0
	for i in images:
		if i.id == image_id:
			flag =1
			break
		
    	if flag == 1:
		__conn__.deregister_image(image_id)
	else:
		print "Please enter Valid image id "        
	return 1


# Print Images List
def get_image_list():
	global __conn__

	images = __conn__.get_all_images()
	#print "------------------ Image List ------------------\n"
	#print "-----ID-----|---------------Name---------------|\n"
	
	for i in images:
    		print i.id + '|' + i.name
		print "\n"


# Get Ubuntu Specific Image
def get_os_specific_image():
	global __conn__
	global image

	image = __conn__.get_all_images(filters={'name':'eustore-trusty-server-cloudimg-amd64_tar'})
	print image[0].id + '|' + image[0].name 
	print "\n"
	return image[0].id


# Get particular specs Images
def get_specs_image():
	global __conn__
	global image

	image = conn.get_all_images(filters={'architecture':'x86_64'})
	print image[0].id + '|' + image[0].name 
	print "\n"
	return image[0].id


# Creating Instance
def create_instance(image,key,group,instance):
	global __conn__
	try:
		reservation = __conn__.run_instances(image_id=image,key_name=key, security_groups=None, instance_type=instance)
		print reservation
		instance = reservation.instances[0]
		print "Instannce hele"
		print "\n"
		print 'Waiting for instance to create...'

		while instance.state != 'running':
			time.sleep(5)
    			instance.update()
		print 'Instance is now running' 
		print 'Instance Public IP : %s' % instance.__dict__.get('ip_address')
		print 'Instance Private IP : %s' % instance.__dict__.get('private_ip_address')
		print 'Instance ID : %s' % instance.__dict__.get('id')
		print 'Instance Type : %s' % instance.__dict__.get('instance_type')
		print 'Instance Image ID : %s' % instance.__dict__.get('image_id')
		print "\n"
		return instance.__dict__.get('id')
  	except:
		print "Please enter valid values to creaet instance"
		sys.exit()

# Validating Instance
def validate_instance(instance_id):
	global __conn__
        ins_id = __conn__.get_only_instances(instance_id)
        for i in ins_id:
        	if i.id == instance_id:
                	print "Instance is Valid : Instance ID : %s" % i.id
			return 1
                else: 
			print "Instance is Not Valid : Instance ID : %s" % i.id
			return 0
                     

# Starting Instance
def start_instance(instance_id):
        global __conn__
	
        instance = __conn__.get_only_instances(instance_id)
	if instance:
		start = __conn__.start_instances(instance[0].id)
		print 'Waiting for instance to start...'
		while instance[0].state != 'running':
	    		time.sleep(5)
	    		instance[0].update()
		print 'Instance has now been started' 
		print 'Started Instance IP is %s' % instance[0].ip_address
		print 'Started Instance ID is %s' % instance[0].id
		return instance[0].state
	else:
		print " Please enter valid instance id.."
		return 0
		

# Stopping Instance
def stop_instances(instance_id):
        global __conn__
	try:
        	instance = __conn__.get_only_instances(instance_id)
		stop = __conn__.stop_instances(instance[0].id)	
		print 'Waiting for instance to stop...'
	
		while instance[0].state != 'stopped':
    			time.sleep(5)
    			instance[0].update()
		print 'Instance has now been stoped' 
		print 'Stopped Instance IP is %s' % instance[0].ip_address
		print 'Stopped Instance ID is %s' % instance[0].id
	except :
		print " Please enter valid instance id.."
		return 0
	


# Copying Instance
#def clone_instance():


# Restart Instance
def restart_instance(instance_id):
        global __conn__
        reservation = __conn__.get_only_instances(instance_id)
	instance = reservation.instances[0]
	start = __conn__.reboot_instances(instance.id)
	print 'Waiting for instance to start...'
	
	while instance.state != 'running':
    		time.sleep(5)
    		instance.update()
	print 'Instance has now been rebooted' 
	print 'Rebooted Instance IP is %s' % instance.ip_address
	print 'Rebooted Instance ID is %s' % instance.id


# Destroy Instance
def terminate_instance(instance_id):
	global __conn__

	instance = __conn__.get_only_instances(instance_id)
	if instance:
		terminate = __conn__.terminate_instances(instance[0].id)
		print 'waiting for instance to terminate...'
	
		while instance[0].state != 'terminated':
    			time.sleep(5)
    			instance[0].update()
		print 'Instance has now been terminated' 
		return instance.state
	else:
		print ' Please Enter Valid Value of Instance id '
		return 0



# Instance Status
def get_status(instance_id):
	global __conn__
        instance = __conn__.get_only_instances(instance_id)
	print "Instance Status: %s" % instance[0].state



# Instance IP
def get_instance_ip(instance_id):
	global __conn__
        instance = __conn__.get_only_instances(instance_id)
	print "Instance IP: %s" % instance[0].ip_address


# Running Instance
def get_running_instances():
	global __conn__
        instance = __conn__.get_only_instances()
	for i in instance:
		if i.state == 'running':
                        print "Yes"
			running_instance.add(i)
	return running_instance


# Volume Snapshot
def snapshot_instance(volume_id):
	snapshot = conn.create_snapshot(volume_id, 'Snap_shot'+volume_id)
	return 'Snap_shot'+volume_id


# List of Running Instances
def get_all_instances():
	global __conn__
	instance = conn.get_only_instances()
	for i in instance:
		if i.state == 'running':
			running_instance.add(i)
	return running_instance


def get_ram_size_instance(instance_id):
	global __conn__
        global x
     	for i in range(len(name)):
        	if type == name[i]:
                	size = ram_size[i]
                        break
        return size


#def change_ram_size_instance():

def get_disk_size_instance(instance_id):
	global __conn__
        global x
     	for i in range(len(name)):
        	if type == name[i]:
                	size = disk_size[i]
                        break
        return size

#def change_disk_size_instance():

#def get_available_ip_range():

def no_of_instances():
	global __conn__
        instance = __conn__.get_only_instances()
        
	count = len(instance)
	return count


def max_instance_count():
	global __Conn__

#def get_resource_utilization():
	
#def connect_ssh():

#def connect_without_ssh():


def get_all_key_pairs():
	global __conn__
	key = __conn__.get_all_key_pairs() 
	#print "------------------ Key List ------------------\n"

        for k in key:
                print k.name
                print "\n"

def get_key_pair(key_name):
	global __conn__

	k_name = 0
	key = __conn__.get_all_key_pairs()
        for k in key:
                if k.name == key_name:
                        #print "Key Matched"
                        k_name = k
                        break
        if k_name == 0:
                #print "Key not found"
                k_name = __conn__.create_key_pair(key_name)
                #print k_name
	return k_name


def get_all_security_groups():
	global __conn__
        group = __conn__.get_all_security_groups()
        print "------------------ Group List ------------------\n"

        for g in group:
                print g.name
                print "\n"


def get_group(group_name):
	global __conn__

	g_name = 0
	group = __conn__.get_all_security_groups() 
	for g in group:
    		if g.name == group_name:
        		#print "Group Matched"
       			g_name = g
        		break
	if g_name == 0:  
    		#print "Group not found"
    		g_name = __conn__.create_group(group_name)
		#print "Created New Group"
    		#print g_name 
	return g_name

def find_instance():
	global __conn__
	rs = __conn__.get_all_instances()

	for i in rs:
		print rs[i].get('id')



